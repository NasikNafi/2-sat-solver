#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <stack>
#include <chrono>

using namespace std;

int nVar;
vector<vector<int>> *gp = NULL;
vector<vector<int>> *gpRev = NULL;
vector<vector<int>> adjList;
vector<vector<int>> adjListRev;

bool *visited = NULL;
stack<int> s;
int *scc_of_node = NULL;
int cur_scc = 0;

int construct_graph_from_input(char *fileName ) {
    ifstream iFile(fileName);
    char temp[10];
    string str;

    // reading comments and parameters
    streampos oldpos;
    while (getline(iFile, str) && (str[0] == 'c' || str[0] == 'p')) {
        oldpos = iFile.tellg();
        if (str[0] == 'p') {
            string buf;
            stringstream ss(str);
            vector<string> tokens;
            while (ss >> buf)
                tokens.push_back(buf);
            nVar = stoi(tokens[tokens.size() - 2]);
        }
    }

    gp = new vector<vector<int>>(nVar * 2 + 1);
    adjList = *gp;
    gpRev = new vector<vector<int>>(nVar * 2 + 1);
    adjListRev = *gpRev;

    visited = new bool[nVar * 2 + 1] {false};
    scc_of_node = new int[nVar * 2 + 1] {0};

    // reading clauses and graph construction
    iFile.seekg (oldpos);
    while (iFile >> temp) {
        // reading a clause with terminating 0
        int x, y, xP, yP;
        x = atoi(temp);
        iFile >> temp;
        y = atoi(temp);
        if(y == 0){
            y = x;
        }else{
            iFile >> temp;
            if (temp[0] != '0') {
                cout << "Input not in correct format";
                return -1;
            }
        }

        // mapping the literals for graph construction
        if (x < 0) {
            xP = -x;
            x = nVar - x;
        } else {
            xP = nVar + x;
        }
        if (y < 0) {
            yP = -y;
            y = nVar - y;
        } else {
            yP = nVar + y;
        }

        // adding edges to the graph
        adjList[xP].push_back(y);
        adjList[yP].push_back(x);

        // adding edges to reverse graph
        adjListRev[y].push_back(xP);
        adjListRev[x].push_back(yP);
    }
    iFile.close();
    return 0;
}

void dfs_on_rev(int u) {
    if(visited[u])
        return;
    visited[u] = true;
    for (int i = 0; i < adjListRev[u].size(); ++i) {
        dfs_on_rev(adjListRev[u][i]);
    }
    s.push(u);
}

void dfs(int u) {
    if(visited[u])
        return;
    visited[u] = true;
    for (int i = 0; i < adjList[u].size(); ++i) {
        dfs(adjList[u][i]);
    }
    scc_of_node[u] = cur_scc;
}

int main(int argc, char *argv[]) {

    auto t1 = chrono::high_resolution_clock::now();
    // getting command line argument
    char fileName[100];
    if (argc == 2) {
        strncpy(fileName, argv[1], 100);
    } else {
        cout << "Provide required number of arguments correctly." << endl;
    }

    if(construct_graph_from_input(fileName)==-1){
        return 0;
    }

    for (int i = 1; i <= 2 * nVar; ++i) {
        if(!visited[i]){
            dfs_on_rev(i);
        }
    }

    for (int i = 1; i <= 2 * nVar; ++i) {
        visited[i] = false;
    }

    while (!s.empty()){
        int u = s.top();
        s.pop();

        if(!visited[u]){
            cur_scc++;
            dfs(u);
        }
    }

    bool res = true;
    for (int i = 1; i <= nVar; ++i) {
        if(scc_of_node[i]==scc_of_node[i+nVar]){
            res = false;
            break;
        }
    }

    auto t2 = chrono::high_resolution_clock::now();
    chrono::duration<double,milli> elapsed = t2 - t1;

    ofstream result_file("result.txt");
    if (result_file.is_open()) {
        result_file << "Filename: " << fileName << "    ";
        result_file << " ";
        if(res){
            result_file << "SAT  ";
        }else{
            result_file << "UnSAT";
        }
        result_file << "    Runtime: " << elapsed.count() << " millis\n";
    } else cout << "Unable to open file";
    result_file.close();

    //freeing memory
    delete gp;
    gp = NULL;
    delete gpRev;
    gpRev = NULL;

    delete visited;
    visited = NULL;
    delete scc_of_node;
    scc_of_node = NULL;
    return 0;
}